const mongoose = require("mongoose");

let Schema = mongoose.Schema;

let PizzaSchema = new Schema(
  {
    name: {
      type: String,
    },
    ingredients: [String],
  },
  {
    timestamps: true,
  }
);

let pizzaSchema = mongoose.model("Pizza", PizzaSchema);

module.exports = pizzaSchema;
