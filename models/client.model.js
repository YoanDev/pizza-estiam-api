const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const tokenSecret = process.env.TOKEN_SECRET;

let Schema = mongoose.Schema;

let ClientSchema = new Schema(
  {
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    email: {
      type: String,
    },
    password: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

ClientSchema.pre("save", async function (next) {
  if (this.isModified("password") || this.isNew) {
    this.password = bcrypt.hashSync(this.password, 8);
  } else {
    return next();
  }
});

ClientSchema.methods.comparePassword = function (pw) {
  return bcrypt.compareSync(pw, this.password);
};

ClientSchema.methods.getJWT = function () {
  return jwt.sign({ id: this._id.toString() }, tokenSecret, {
    expiresIn: "1h",
  });
};

ClientSchema.methods.toClient = function () {
  return {
    firstName: this.firstName,
    lastName: this.lastName,
    email: this.email,
  };
};

let clientSchema = mongoose.model("Client", ClientSchema);

module.exports = clientSchema;
