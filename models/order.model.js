const mongoose = require("mongoose");

let Schema = mongoose.Schema;

let OrderSchema = new Schema(
  {
    restaurant: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Restaurant",
    },
    employees: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Employee",
      },
    ],
    clients: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Client",
      },
    ],
    pizzas: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Pizza",
      },
    ],
  },
  {
    timestamps: true,
  }
);

let orderSchema = mongoose.model("Order", OrderSchema);

module.exports = orderSchema;
