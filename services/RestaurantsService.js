class RestaurantsService {
  constructor({ Restaurant, User }) {
    this.restaurantModel = Restaurant;
    this.userModel = User;
  }

  async creatRestaurant(data) {
    // Check if restaurant exists
    let _restaurant = await this.restaurantModel.findOne({
      name: data.name,
    });

    if (_restaurant) {
      return {
        message: "Failed! Restaurant already exists!",
      };
    } else {
      _restaurant = new this.restaurantModel(data);
      await _restaurant.save();

      return {
        message: `Restaurant (${data.name}) created successfully!`,
      };
    }
  }

  async listRestaurants() {
    let _restaurants = await this.restaurantModel.find().exec();

    return {
      restaurants: _restaurants,
    };
  }

  async listOneRestaurant(restaurantId) {
    let _restaurant = await this.restaurantModel
      .findById(restaurantId)
      .populate("employees")
      .exec();

    if (_restaurant) {
      return {
        restaurant: _restaurant,
      };
    } else {
      return {
        message: "Restaurant Not Found!",
      };
    }
  }

  async updateRestaurant(restaurantId, data) {
    // Check if restaurant exists
    let _restaurant = await this.restaurantModel.findById(restaurantId).exec();

    if (_restaurant) {
      _restaurant.name = data.name ? data.name : _restaurant.name;
      _restaurant.city = data.city ? data.city : _restaurant.city;

      _restaurant = await _restaurant.save();

      return {
        message: "Restaurant Updated Successfully!",
        restaurant: _restaurant,
      };
    } else {
      return {
        message: "Failed! Restaurant Not Found!",
      };
    }
  }

  async deleteRestaurant(restaurantId) {
    await this.restaurantModel.findByIdAndRemove(restaurantId);

    return {
      message: "Restaurant Deleted Successfully!",
    };
  }

  async addUsersToRestaurant(restaurantId, data) {
    // Check if restaurant exists
    let _restaurant = await this.restaurantModel.findById(restaurantId).exec();

    if (_restaurant) {
      for (let i = 0; i < data.users.length; i++) {
        let _user = await this.userModel
          .findOne({
            email: data.users[i],
          })
          .exec();

        if (_user) {
          _restaurant.users.push(_user._id);
        }
        await _restaurant.save();
      }

      return {
        message: "Users added to restaurant successfully!",
      };
    } else {
      return {
        message: "Failed! Restaurant Not Found!",
      };
    }
  }
}

module.exports = RestaurantsService;
