class PizzasService {
  constructor({ Pizza }) {
    this.pizzaModel = Pizza;
  }

  async creatPizza(data) {
    console.log("first");
    // Check if pizza exists
    let _pizza = await this.pizzaModel.findOne({
      name: data.name,
    });

    if (_pizza) {
      return {
        message: "Failed! name already in use!",
      };
    } else {
      _pizza = new this.pizzaModel(data);
      console.log(_pizza);
      console.log("first");
      await _pizza.save();

      return {
        message: `Pizza (${data.name}) created successfully!`,
      };
    }
  }

  async listPizzas(paginationData) {
    console.log(paginationData);
    let pizzasList = [];
    let limit = paginationData.limit ?? 10;
    let page = paginationData.page ?? 0;

    let searchQuery = {};

    if (paginationData.name) {
      searchQuery = {
        name: paginationData.name,
      };
    }

    let _pizzas = await this.pizzaModel
      .find(searchQuery)
      .limit(limit)
      .skip(limit * page);
    console.log(
      "🚀 ~ file: PizzasService.js ~ line 43 ~ PizzasService ~ listPizzas ~ _pizzas",
      _pizzas
    );

    for (let i = 0; i < _pizzas.length; i++) {
      pizzasList.push(_pizzas[i]);
    }

    return {
      count: _pizzas.length,
      pizzas: pizzasList,
    };
  }

  async updatePizzas(pizzaId, data) {
    // Check if pizza exists
    let _pizza = await this.pizzaModel.findById(pizzaId).exec();

    if (_pizza) {
      _pizza.name = data.name ? data.name : _pizza.name;
      _pizza.ingredients = data.ingredients
        ? data.ingredients
        : _pizza.ingredients;

      _pizza = await _pizza.save();

      return {
        message: "Pizza Updated Successfully!",
        pizza: _pizza,
      };
    } else {
      return {
        message: "Failed! Pizza Not Found!",
      };
    }
  }

  async deletePizza(pizzaId) {
    await this.pizzaModel.findByIdAndRemove(pizzaId);

    return {
      message: "Pizza Deleted Successfully!",
    };
  }

  async listOnePizza(pizzaId) {
    let _pizza = await this.pizzaModel.findById(pizzaId).exec();

    if (_pizza) {
      return {
        pizza: _pizza,
      };
    } else {
      return {
        message: "Pizza Not Found!",
      };
    }
  }
}

module.exports = PizzasService;
