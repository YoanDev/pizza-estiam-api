class OrdersService {
  constructor({ Order }) {
    this.orderModel = Order;
  }

  async creatOrder(data) {
    await this.orderModel(data).save();

    return {
      message: `Order for this client has been created successfully!`,
    };
  }

  async listOrders(paginationData) {
    console.log(paginationData);
    let ordersList = [];
    let limit = paginationData.limit ?? 10;
    let page = paginationData.page ?? 0;

    let searchQuery = {};

    if (paginationData.name) {
      searchQuery = {
        name: paginationData.name,
      };
    }

    let _orders = await this.orderModel
      .find(searchQuery)
      .limit(limit)
      .skip(limit * page);
    console.log(
      "🚀 ~ file: OrdersService.js ~ line 43 ~ OrdersService ~ listOrders ~ _orders",
      _orders
    );

    for (let i = 0; i < _orders.length; i++) {
      ordersList.push(_orders[i]);
    }

    return {
      count: _orders.length,
      orders: ordersList,
    };
  }

  async updateOrders(orderId, data) {
    // Check if order exists
    let _order = await this.orderModel.findById({ _id: orderId }).exec();

    if (_order) {
      _order.restaurant = data.restaurant ? data.restaurant : _order.restaurant;
      _order.employees = data.employees ? data.employees : _order.employees;
      _order.clients = data.clients ? data.clients : _order.clients;
      _order.pizzas = data.pizzas ? data.pizzas : _order.pizzas;

      _order = await _order.save();

      return {
        message: "Order Updated Successfully!",
        order: _order,
      };
    } else {
      return {
        message: "Failed! Order Not Found!",
      };
    }
  }

  async deleteOrder(orderId) {
    await this.orderModel.findByIdAndRemove(orderId);

    return {
      message: "Order Deleted Successfully!",
    };
  }

  async listOneOrder(orderId) {
    let _order = await this.orderModel
      .findById(orderId)
      .populate(["clients", "pizzas", "employees", "restaurant"])
      .exec();

    if (_order) {
      return {
        order: _order,
      };
    } else {
      return {
        message: "Order Not Found!",
      };
    }
  }
}

module.exports = OrdersService;
