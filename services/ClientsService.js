class ClientService {
  constructor({ Client }) {
    this.clientModel = Client;
  }

  async creatClient(data) {
    // Check if client exists
    let _client = await this.clientModel.findOne({
      email: data.email,
    });

    if (_client) {
      return {
        message: "Failed! Email already in use!",
      };
    } else {
      _client = new this.clientModel(data);
      await _client.save();

      return {
        message: `Client (${data.email}) created successfully!`,
      };
    }
  }

  async listClients(paginationData) {
    let clientsList = [];
    let limit = paginationData.limit || 10;
    let page = paginationData.page || 0;

    let searchQuery = {};

    if (paginationData.name) {
      searchQuery = {
        firstName: paginationData.name,
      };
    }

    let _clients = await this.clientModel
      .find(searchQuery)
      .limit(limit)
      .skip(limit * page);
    console.log(
      "🚀 ~ file: ClientsService.js ~ line 43 ~ ClientService ~ listClients ~ _clients",
      _clients
    );

    for (let i = 0; i < _clients.length; i++) {
      clientsList.push(_clients[i]);
    }

    return {
      count: _clients.length,
      clients: clientsList,
    };
  }

  async updateClients(clientId, data) {
    // Check if client exists
    let _client = await this.clientModel.findById(clientId).exec();

    if (_client) {
      _client.firstName = data.firstName ? data.firstName : _client.firstName;
      _client.lastName = data.lastName ? data.lastName : _client.lastName;
      _client.password = data.password ? data.password : _client.password;

      _client = await _client.save();

      return {
        message: "Client Updated Successfully!",
        client: _client,
      };
    } else {
      return {
        message: "Failed! Client Not Found!",
      };
    }
  }

  async deleteClient(clientId) {
    await this.clientModel.findByIdAndRemove(clientId);

    return {
      message: "Client Deleted Successfully!",
    };
  }

  async login(data) {
    if (data.email && data.password) {
      let _client = await this.clientModel
        .findOne({
          email: data.email,
        })
        .exec();

      if (_client) {
        let isPwdValid = _client.comparePassword(data.password);

        if (isPwdValid) {
          return {
            data: _client,
            token: _client.getJWT(),
          };
        } else {
          return {
            message: "Password is not valid!",
          };
        }
      } else {
        return {
          message: "Client Not Found!",
        };
      }
    } else {
      return {
        message: "Missing Data!!",
      };
    }
  }
}

module.exports = ClientService;
