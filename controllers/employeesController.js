const { Employee } = require("../models");
const EmployeesService = require("./../services/EmployeesService");
const employeesServices = new EmployeesService({ Employee });

const createEmployee = async function (req, res, next) {
  let createEmployeeProcess = await employeesServices.creatEmployee(req.body);

  return res.status(201).send(createEmployeeProcess);
};

const listEmployees = async function (req, res, next) {
  let listEmployeesProcess = await employeesServices.listEmployees(req.query);

  return res.status(200).send(listEmployeesProcess);
};

const updateEmployees = async function (req, res, next) {
  let updateEmployeesProcess = await employeesServices.updateEmployees(
    req.params.employeeId,
    req.body
  );

  return res.status(200).send(updateEmployeesProcess);
};

const deleteEmployee = async function (req, res, next) {
  let deleteEmployeeProcess = await employeesServices.deleteEmployee(
    req.params.employeeId
  );

  return res.status(200).send(deleteEmployeeProcess);
};

const loginEmployee = async function (req, res, next) {
  let loginProcess = await employeesServices.login(req.body);

  return res.status(200).send(loginProcess);
};

module.exports = {
  createEmployee,
  listEmployees,
  updateEmployees,
  deleteEmployee,
  loginEmployee,
};
