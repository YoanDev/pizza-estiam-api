const { Pizza } = require("../models");
const PizzasService = require("../services/PizzasService");
const pizzasService = new PizzasService({ Pizza });

const createPizza = async function (req, res, next) {
  let createPizzaProcess = await pizzasService.creatPizza(req.body);

  return res.status(201).send(createPizzaProcess);
};

const listPizzas = async function (req, res, next) {
  const limit = req.param.limit ?? 10;
  let listPizzasProcess = await pizzasService.listPizzas(limit);

  return res.status(200).send(listPizzasProcess);
};
const updatePizza = async function (req, res, next) {
  let updatePizzaProcess = await pizzasService.updatePizzas(
    req.params.pizzaId,
    req.body
  );

  return res.status(200).send(updatePizzaProcess);
};

const deletePizza = async function (req, res, next) {
  let deletePizzaProcess = await pizzasService.deletePizza(req.params.pizzaId);

  return res.status(200).send(deletePizzaProcess);
};

const listOnePizza = async function (req, res, next) {
  let listOnePizzaProcess = await pizzasService.listOnePizza(
    req.params.pizzaId
  );

  return res.status(200).send(listOnePizzaProcess);
};

module.exports = {
  createPizza,
  listPizzas,
  listOnePizza,
  deletePizza,
  updatePizza,
};
