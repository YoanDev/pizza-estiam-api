const { Order } = require("../models");
const OrdersService = require("../services/OrdersService");
const ordersService = new OrdersService({ Order });

const createOrder = async function (req, res, next) {
  console.log("BBB7");
  let createOrderProcess = await ordersService.creatOrder(req.body);

  return res.status(201).send(createOrderProcess);
};

const listOrders = async function (req, res, next) {
  const limit = req.param.limit ?? 10;
  let listOrdersProcess = await ordersService.listOrders(limit);

  return res.status(200).send(listOrdersProcess);
};
const updateOrder = async function (req, res, next) {
  let updateOrderProcess = await ordersService.updateOrders(
    req.params.orderId,
    req.body
  );

  return res.status(200).send(updateOrderProcess);
};

const deleteOrder = async function (req, res, next) {
  let deleteOrderProcess = await ordersService.deleteOrder(req.params.orderId);

  return res.status(200).send(deleteOrderProcess);
};

const listOneOrder = async function (req, res, next) {
  let listOneOrderProcess = await ordersService.listOneOrder(
    req.params.orderId
  );

  return res.status(200).send(listOneOrderProcess);
};

module.exports = {
  createOrder,
  listOrders,
  listOneOrder,
  deleteOrder,
  updateOrder,
};
