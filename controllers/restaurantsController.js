const { Restaurant, User } = require("../models");
const RestaurantsService = require("../services/RestaurantsService");
const restaurantsService = new RestaurantsService({ Restaurant, User });

const creatRestaurant = async function (req, res, next) {
  let creatRestaurantProcess = await restaurantsService.creatRestaurant(
    req.body
  );

  return res.status(201).send(creatRestaurantProcess);
};

const listRestaurants = async function (req, res, next) {
  let listRestaurantsProcess = await restaurantsService.listRestaurants();

  return res.status(200).send(listRestaurantsProcess);
};

const updateRestaurant = async function (req, res, next) {
  let updateRestaurantProcess = await restaurantsService.updateRestaurant(
    req.params.restaurantId,
    req.body
  );

  return res.status(200).send(updateRestaurantProcess);
};

const deleteRestaurant = async function (req, res, next) {
  let deleteRestaurantProcess = await restaurantsService.deleteRestaurant(
    req.params.restaurantId
  );

  return res.status(200).send(deleteRestaurantProcess);
};

const addUsersToRestaurant = async function (req, res, next) {
  let addUsersToRestaurantProcess =
    await restaurantsService.addUsersToRestaurant(
      req.params.restaurantId,
      req.body
    );

  return res.status(200).send(addUsersToRestaurantProcess);
};

const listOneRestaurant = async function (req, res, next) {
  let listOneRestaurantProcess = await restaurantsService.listOneRestaurant(
    req.params.restaurantId
  );

  return res.status(200).send(listOneRestaurantProcess);
};

module.exports = {
  creatRestaurant,
  listRestaurants,
  updateRestaurant,
  deleteRestaurant,
  addUsersToRestaurant,
  listOneRestaurant,
};
