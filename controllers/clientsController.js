const { Client } = require("../models");
const ClientsService = require("./../services/ClientsService");
const clientsServices = new ClientsService({ Client });

const createClient = async function (req, res, next) {
  let createClientProcess = await clientsServices.creatClient(req.body);

  return res.status(201).send(createClientProcess);
};

const listClients = async function (req, res, next) {
  let listClientsProcess = await clientsServices.listClients(req.query);

  return res.status(200).send(listClientsProcess);
};

const updateClients = async function (req, res, next) {
  let updateClientsProcess = await clientsServices.updateClients(
    req.params.clientId,
    req.body
  );

  return res.status(200).send(updateClientsProcess);
};

const deleteClient = async function (req, res, next) {
  let deleteClientProcess = await clientsServices.deleteClient(
    req.params.clientId
  );

  return res.status(200).send(deleteClientProcess);
};

const loginClient = async function (req, res, next) {
  let loginProcess = await clientsServices.login(req.body);

  return res.status(200).send(loginProcess);
};

module.exports = {
  createClient,
  listClients,
  updateClients,
  deleteClient,
  loginClient,
};
