const express = require("express");
const router = express.Router();

const employeesController = require("../controllers/employeesController");
const authJwt = require("../middlewares/authJwt");

/**
 * @api {post} /employees Create a new employee
 * @apiName Create Employee
 * @apiGroup Employee
 *
 * @apiSuccess {String} firstname Firstname of the Employee.
 * @apiSuccess {String} lastname  Lastname of the Employee.
 */
router.post(
  "/employees",
  [authJwt.verifyToken],
  employeesController.createEmployee
);

/**
 * @api {get} /employees List all employees
 * @apiName List Employees
 * @apiGroup Employee
 *
 * @apiSuccess {String} firstname Firstname of the Employee.
 * @apiSuccess {String} lastname  Lastname of the Employee.
 */
router.get(
  "/employees",
  [authJwt.verifyToken],
  employeesController.listEmployees
);

/**
 * @api {put} /employees/:employeeId Update a employee
 * @apiName Update Employee
 * @apiGroup Employee
 *
 * @apiSuccess {String} firstname Firstname of the Employee.
 * @apiSuccess {String} lastname  Lastname of the Employee.
 */
router.put(
  "/employees/:employeeId",
  [authJwt.verifyToken],
  employeesController.updateEmployees
);

/**
 * @api {delete} /employees/:employeeId Delete a employee
 * @apiName Update Employee
 * @apiGroup Employee
 *
 * @apiSuccess {String} firstname Firstname of the Employee.
 * @apiSuccess {String} lastname  Lastname of the Employee.
 */
router.delete(
  "/employees/:employeeId",
  [authJwt.verifyToken],
  employeesController.deleteEmployee
);

/**
 * @api {post} /login Login a employee
 * @apiName Update Employee
 * @apiGroup Employee
 *
 * @apiSuccess {String} firstname Firstname of the Employee.
 * @apiSuccess {String} lastname  Lastname of the Employee.
 */
router.post("/employees/login", employeesController.loginEmployee);

module.exports = router;
