const express = require("express");
const router = express.Router();

const pizzasController = require("../controllers/pizzasController");

router.post("/pizzas", pizzasController.createPizza);

router.get("/pizzas", pizzasController.listPizzas);

router.put("/pizzas/:pizzaId", pizzasController.updatePizza);

router.delete("/pizzas/:pizzaId", pizzasController.deletePizza);

router.get("/pizzas/:pizzaId", pizzasController.listOnePizza);

module.exports = router;
