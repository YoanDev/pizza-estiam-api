const express = require("express");
const router = express.Router();

const clientsController = require("../controllers/clientsController");
const authJwt = require("../middlewares/authJwt");

/**
 * @api {post} /clients Create a new client
 * @apiName Create Client
 * @apiGroup Client
 *
 * @apiSuccess {String} firstname Firstname of the Client.
 * @apiSuccess {String} lastname  Lastname of the Client.
 */
router.post("/clients", [authJwt.verifyToken], clientsController.createClient);

/**
 * @api {get} /clients List all clients
 * @apiName List Clients
 * @apiGroup Client
 *
 * @apiSuccess {String} firstname Firstname of the Client.
 * @apiSuccess {String} lastname  Lastname of the Client.
 */
router.get("/clients", [authJwt.verifyToken], clientsController.listClients);

/**
 * @api {put} /clients/:clientId Update a client
 * @apiName Update Client
 * @apiGroup Client
 *
 * @apiSuccess {String} firstname Firstname of the Client.
 * @apiSuccess {String} lastname  Lastname of the Client.
 */
router.put(
  "/clients/:clientId",
  [authJwt.verifyToken],
  clientsController.updateClients
);

/**
 * @api {delete} /clients/:clientId Delete a client
 * @apiName Update Client
 * @apiGroup Client
 *
 * @apiSuccess {String} firstname Firstname of the Client.
 * @apiSuccess {String} lastname  Lastname of the Client.
 */
router.delete(
  "/clients/:clientId",
  [authJwt.verifyToken],
  clientsController.deleteClient
);

/**
 * @api {post} /login Login a client
 * @apiName Update Client
 * @apiGroup Client
 *
 * @apiSuccess {String} firstname Firstname of the Client.
 * @apiSuccess {String} lastname  Lastname of the Client.
 */
router.post("/clients/login", clientsController.loginClient);

module.exports = router;
