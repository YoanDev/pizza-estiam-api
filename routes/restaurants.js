const express = require("express");
const router = express.Router();

const restaurantsController = require("../controllers/restaurantsController");

router.post("/restaurants", restaurantsController.creatRestaurant);

router.get("/restaurants", restaurantsController.listRestaurants);

router.put(
  "/restaurants/:restaurantId",
  restaurantsController.updateRestaurant
);

router.delete(
  "/restaurants/:restaurantId",
  restaurantsController.deleteRestaurant
);

router.post(
  "/restaurants/:restaurantId/addUsers",
  restaurantsController.addUsersToRestaurant
);

router.get(
  "/restaurants/:restaurantId",
  restaurantsController.listOneRestaurant
);

module.exports = router;
