const express = require("express");
const router = express.Router();

const ordersController = require("../controllers/ordersController");

router.post("/orders", ordersController.createOrder);

router.get("/orders", ordersController.listOrders);

router.put("/orders/:orderId", ordersController.updateOrder);

router.delete("/orders/:orderId", ordersController.deleteOrder);

router.get("/orders/:orderId", ordersController.listOneOrder);

module.exports = router;
